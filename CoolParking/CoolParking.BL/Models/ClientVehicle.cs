﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public class ClientVehicle
    {
        public string Id { get; set; }
        public decimal Balance { get; set; }
        public int VehicleType { get; set; }
    }
}
