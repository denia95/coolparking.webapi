﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
     public class Parking
     {
        
        public List<Vehicle> Vehicles { get; }

        private static Parking _instance;

        public decimal Balance { get; set; }

        private Parking()
        {
            Balance = Settings.InitialBalanceOfParking;
            Vehicles = new List<Vehicle>(Settings.ParkingCapacity);
        }

        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }
        
     }
}