﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : Controller
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        public IActionResult Index()
        {
            return View();
        }

        //GET api/parking/freePlaces
        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }

        //GET api/parking/capacity
        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        //GET api/parking/balance
        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return _parkingService.GetBalance();
        }
    }
}
