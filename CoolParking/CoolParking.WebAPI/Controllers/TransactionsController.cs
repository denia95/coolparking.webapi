﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        //GET api/transactions/last
        [HttpGet]
        [Route("last")]
        public string GetLast()
        {
            return (_parkingService as ParkingService).GetLastTransactions(); ;
        }

        //GET api/transactions/all
        [HttpGet]
        [Route("all")]
        public string GetAll()
        {
            var a = _parkingService.ReadFromLog();
            return a;
        }

        //Put api/transactions/topUpVehicle
        [HttpPut]
        [Route("topUpVehicle")]
        public void TopUpVehicle(ClientVehicle clientvehicle)
        {
            VehicleType vType = (VehicleType)clientvehicle.VehicleType;
            Vehicle vehicle = new Vehicle(clientvehicle.Id, vType , clientvehicle.Balance);
            _parkingService.AddVehicle(vehicle);
        }
    }
}
