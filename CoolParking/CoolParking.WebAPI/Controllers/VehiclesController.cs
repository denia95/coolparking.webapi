﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        private IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        //GET api/vehicles
        [HttpGet]
        public IReadOnlyCollection<ClientVehicle> Get()
        {
            var vehicles = _parkingService.GetVehicles();
            List<ClientVehicle> response = new List<ClientVehicle>();

            foreach(var item in vehicles)
            {
                ClientVehicle v = new ClientVehicle();
                v.Balance = item.Balance;
                v.Id = item.Id;
                v.VehicleType = (int) item.VehicleType;
                response.Add(v);
            }

            return response;
        }

        //GET api/vehicles/id
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            try
            {
                Vehicle.Validate(id, 0);
                var vehicles = _parkingService.GetVehicles();
                var vehicle = vehicles.FirstOrDefault(x => x.Id == id);
                if (vehicle == null)
                {
                    return NotFound();
                }

                ClientVehicle response = new ClientVehicle { Id = vehicle.Id, Balance = vehicle.Balance, VehicleType = (int)vehicle.VehicleType };
                return new ObjectResult(response);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        //POST api/vehicles
        [HttpPost]
        public IActionResult Post([FromBody] ClientVehicle clientVehicle)
        {
            VehicleType vType = (VehicleType)clientVehicle.VehicleType - 1;

            try
            {
                Vehicle vehicle = new Vehicle(clientVehicle.Id, vType, clientVehicle.Balance);
                _parkingService.AddVehicle(vehicle);
                return Created("", clientVehicle);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        //DELETE api/vehicles/id
        [HttpDelete ("{id}")]
        public void Delete( string Id)
        {
            _parkingService.RemoveVehicle(Id);
        }
    }
}
