﻿using Newtonsoft.Json;

namespace CoolParking.Models
{
    public class Vehicle
    {
        public string Id { get;  set; }

        public decimal Balance { get;  set; }

        public int VehicleType { get;  set; }
    }
}
