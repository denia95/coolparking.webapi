﻿using CoolParking.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;

namespace CoolParking
{
    public class Program
    {
        private const string APP_PATH = "https://localhost:5001/api/";
        static void Main(string[] args)
        {
            ConsoleKeyInfo keyinfo;
            Console.WriteLine("Hello Neo! You are in my Parking Program.");
            do
            {
                Console.WriteLine();
                Console.WriteLine("Choose the options below you want to trigger:");
                Console.WriteLine("1 - Get current Balance of Parking");
                Console.WriteLine("2 - Get Vehicle by id");
                Console.WriteLine("3 - Get free places");
                Console.WriteLine("4 - Show last parking transactions");
                Console.WriteLine("5 - Show history of all transactions");
                Console.WriteLine("6 - Show all vehicles on parking");
                Console.WriteLine("7 - Add Vehicle to Parking");
                Console.WriteLine("8 - Remove Vehicle from Parking");
                Console.WriteLine("9 - Top Up the vehicle");
                Console.WriteLine("0 - Get capacity");
                Console.WriteLine();

                keyinfo = Console.ReadKey();
                try
                {
                    switch (keyinfo.Key)
                    {
                        case ConsoleKey.D1:
                            Console.WriteLine();
                            Console.WriteLine($"Current balance of parking is:  + {GetBalance()}"); 
                            break;
                        case ConsoleKey.D2:
                            GetVehicle();
                            break;
                        case ConsoleKey.D3:
                            Console.WriteLine();
                            Console.WriteLine($"Parking has {GetFreePlaces()} free places."); 
                            break;
                        case ConsoleKey.D4:
                            Console.WriteLine();
                            GetLastTransactions();
                            break;
                        case ConsoleKey.D5:
                            Console.WriteLine();
                            GetAllTransactions();
                            break;
                        case ConsoleKey.D6:
                            Console.WriteLine();
                            GetVehicles();
                            break;
                        case ConsoleKey.D7:
                            Console.WriteLine();
                            AddVehicle();
                            break;
                        case ConsoleKey.D8:
                            Console.WriteLine();
                            DeleteVehicle();
                            break;
                        case ConsoleKey.D9:
                            Console.WriteLine();
                            TopUpVehicle();
                            Console.WriteLine("Balance was increased successfully");
                            break;
                        case ConsoleKey.D0:
                            Console.WriteLine();
                            Console.WriteLine($"Parking has {GetCapacity()} "); 
                            break;
                        default:
                            Console.WriteLine();
                            Console.WriteLine("Please choose number from 0-9");
                            break;
                    }
                }
                catch (ArgumentException e) { Console.WriteLine(); Console.WriteLine(e.Message); }
                catch (InvalidOperationException e) { Console.WriteLine(); Console.WriteLine(e.Message); }
                catch (Exception e) { }
            }
            while (keyinfo.Key != ConsoleKey.Escape);
        }
     
        private static string GetId()
        {
            Console.WriteLine();
            Console.WriteLine("Input Id of Vehicle: ");
            return Console.ReadLine();
        }

        private static int GetFreePlaces()
        {
            int res = 0;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "parking/freePlaces").Result;

                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    res = JsonConvert.DeserializeObject<int>(json);
                }
            }

            return res;
        }

        private static int GetCapacity()
        {
            int res = 0;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "parking/capacity").Result;

                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    res = JsonConvert.DeserializeObject<int>(json);
                }
            }

            return res;
        }

        private static decimal GetBalance()
        {
            int res = 0;
            using(var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "parking/balance").Result;

                if(response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    res = JsonConvert.DeserializeObject<int>(json);
                }
            }
            return res;
        }

        private static void GetVehicles()
        {
            var veh = new List<Vehicle>();
            using(var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "vehicles").Result;
                if(response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    veh = JsonConvert.DeserializeObject<List<Vehicle>>(json);
                }
            }
            Console.WriteLine();
            foreach (var item in veh)
            {
                Console.WriteLine($"Id = {item.Id}, Balance = {item.Balance}, VehicleType = {item.VehicleType}");
            }
        }

        private static void GetVehicle()
        {
            string id = GetId();
            var veh = new Vehicle();
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "vehicles/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    veh = JsonConvert.DeserializeObject<Vehicle>(json);
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Id = {veh.Id}, Balance = {veh.Balance}, VehicleType = {veh.VehicleType}");
        }

        private static void GetLastTransactions()
        {
            string res = string.Empty;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "transactions/last").Result;

                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                }
            }
            Console.WriteLine();
            Console.WriteLine(res);
        }

        private static void GetAllTransactions()
        {
            string res = string.Empty;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(APP_PATH + "transactions/all").Result;

                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                }
            }
            Console.WriteLine();
            Console.WriteLine(res);
        }

        public static void AddVehicle()
        {
            Vehicle vehicle = new Vehicle();

            try
            {
                string id = GetId();
                vehicle.Id = id;
                bool isVehicleType = false;
                Console.WriteLine();
                Console.WriteLine("Input Type of Vehicle: ");
                do {
                    Console.WriteLine("Possible types are: 1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle");
                    var keyinfo = Console.ReadKey();
                    switch (keyinfo.Key)
                    {
                        case ConsoleKey.D1:
                        case ConsoleKey.NumPad1:
                            vehicle.VehicleType = 1;
                            isVehicleType = true;
                            break;
                        case ConsoleKey.D2:
                        case ConsoleKey.NumPad2:
                            vehicle.VehicleType = 2;
                            isVehicleType = true;
                            break;
                        case ConsoleKey.D3:
                        case ConsoleKey.NumPad3:
                            vehicle.VehicleType = 3;
                            isVehicleType = true;
                            break;
                        case ConsoleKey.D4:
                        case ConsoleKey.NumPad4:
                            vehicle.VehicleType = 4;
                            isVehicleType = true;
                            break;
                        default: Console.WriteLine("Wrong Key. Input one of numbers listed below please");
                            break;
                    }
                } while (!isVehicleType);
                Console.WriteLine();
                Console.WriteLine("Input Balance of Vehicle: ");
                var balance = Convert.ToDecimal(Console.ReadLine());
                vehicle.Balance = balance;
            }
            catch (ArgumentException ae)
            {
                Console.WriteLine();
                Console.WriteLine(ae.Message);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Console.WriteLine("Balance is not correct. Please input vehicle details again.");
                throw;
            }

            AddVehicleRequest(vehicle);
        }

        private static void AddVehicleRequest(Vehicle vehicle)
        {
            using (var client = new HttpClient())
            {
                var response = client.PostAsJsonAsync(APP_PATH + "vehicles", vehicle).Result;
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine();
                    Console.WriteLine("Vehicle added successfully");
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Body is invalid");
                }
            }
        }

        private static string TopUpVehicleRequest(Vehicle vehicle)
        {
            using (var client = new HttpClient())
            {
                var response = client.PutAsJsonAsync(APP_PATH + "transactions/topUpVehicle", vehicle).Result;
                if (response.IsSuccessStatusCode)
                {
                    return "Vehicle Balance changhed successfully";
                }
                else
                {
                    return "wrong id";
                }
            }
        }

        public static string TopUpVehicle()
        {
            Vehicle vehicle = new Vehicle();

            try
            {
                string id = GetId();
                vehicle.Id = id;

                Console.Write("Input Balance of Vehicle: ");
                var balance = Convert.ToDecimal(Console.ReadLine());
                vehicle.Balance = balance;
            }
            catch (ArgumentException ae)
            {
                Console.WriteLine();
                Console.WriteLine(ae.Message);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Console.WriteLine("Balance is not correct. Please input vehicle details again.");
                throw;
            }

            string response = TopUpVehicleRequest(vehicle);

            return response;
        }

        private static void DeleteVehicle()
        {
            string id = GetId();
           
            using (var client = new HttpClient())
            {
                var response = client.DeleteAsync(APP_PATH + "vehicles/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine();
                    Console.WriteLine($"{id} was deleted");
                }
            }
        }
      
    }
}
